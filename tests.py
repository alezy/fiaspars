#!/usr/bin/python3
# -*- coding: utf-8 -*-
__author__ = 'aweal'

import fiaspars
import unittest

valid_fias_path = 'd:/FIAS/scheme/'

#    '/home/aweal/FIAS/'

import logging


_name_db = 'AS_ADDROBJ'

__dic_exmpl__ = {'AOGUID': 'exmpl', 'FORMALNAME': 'exmpl',
    'REGIONCODE': 'exmpl', 'AUTOCODE': 'exmpl', 'AREACODE': 'exmpl',
    'CITYCODE': 'exmpl', 'CTARCODE': 'exmpl', 'PLACECODE': 'exmpl',
    'UPDATEDATE': 'exmpl', 'SHORTNAME': 'exmpl', 'AOLEVEL': 'exmpl',
    'AOID': 'exmpl', 'ACTSTATUS': 'exmpl', 'CENTSTATUS': 'exmpl',
    'STARTDATE': 'exmpl', 'ENDDATE': 'exmpl', 'LIVESTATUS': 'exmpl',
    'EXTRCODE': 'exmpl', 'SEXTCODE': 'exmpl', 'OPERSTATUS': 'exmpl',
    'CURRSTATUS': 'exmpl'}


class TestParser(unittest.TestCase):
    def test_valid_path(self):
        'Не существующий путь'
        with self.assertRaises(SystemExit) as cm:
            fp = fiaspars.FiasParser('test_invalid_path')
        self.assertEqual(cm.exception.code, 1)

    def test_exist_path(self):
        'Существующий путь без фиасов'
        fp = fiaspars.FiasParser('d:/FIAS/')
        self.assertIsNone(fp.show_sql())

    def test_show_sql(self):
        'Существующий с фиасом -sql '+_name_db
        fp = fiaspars.FiasParser(valid_fias_path)
        sql = fp.show_sql()
        assert len(sql) > 110

    def test_show_column_valid(self):
        'количество символов примерное значение...'
        columns = list()
        columns.append('HOUSE')
        fp = fiaspars.FiasParser(valid_fias_path)
        col = fp.show_columns([['HOUSE',],])
        self.assertEqual(len(col), 244, 'кол-во символов 244')

        col = fp.show_columns([['HOUSE'], ['ADDROBJ'],])
        self.assertEqual(len(col), 628, 'кол-во строк 628')

        col = fp.show_columns([[]])
        self.assertEqual(len(col), 2031)

    def test_show_column(self):
        'Неправильная таблица...'
        fp = fiaspars.FiasParser(valid_fias_path)
        self.assertEquals(fp.show_columns([['nocolumn',],]), '')

    def test_custom_culumns(self):
        fp = fiaspars.FiasParser(valid_fias_path)
        fp.create_fill_table('Notable', 'Nocolumn')



if __name__ == '__main__':
    unittest.main()



