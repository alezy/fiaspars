#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import xml.sax
import os


class ColumnInfo():
    def __init__(self, name, use, desc, type_column, length):
        self.name = name
        self.use = use
        self.desc = desc
        self.type_clmn = type_column
        self.length = length


class FiasXdsParser():
    def __init__(self, xds_file_name, logger):
        self.name_trigger = ''
        self.arr_column = []
        if not logger is None:
            self.lgr = logger.getChild('xdsparsing')
        self.parsed = False

        if not os.path.exists(xds_file_name) or xds_file_name is None:
            self.lgr.critical('Oh, Where is xds file? -> %s ', xds_file_name)
            exit(0)
        else:
            self.xds = xds_file_name

    def __parsXds__(self):
        parser = xml.sax.make_parser()
        handle = FiasXdsHandler(self.lgr)
        parser.setContentHandler(handle)
        parser.parse(open(self.xds, 'r', encoding='utf-8'))
        self.name_trigger = handle.name_element
        self.arr_column = handle.arr_column
        self.parsed = True

    def getAllcolumns(self):
        self.__parsXds__()
        return self.arr_column

    def getTriggerName(self):
        if self.parsed:
            return self.name_trigger
        else:
            self.lgr.critical('Halt: no trigger_key on FiasXdsParser')


class FiasXdsHandler(xml.sax.ContentHandler):
    def __init__(self, lgr):
        self.arr_column = []
        self.name_element = ''
        self.name_column = ''
        self.use_column = ''
        self.description = ''
        self.type_column = ''
        self.length_column = ''
        self.node_name = ''
        self.attrs = ''

        self.check_data = False
        self.lgr = lgr
        self.begin_data = False

    def get_out_char(self, come_on):
        return ' '.join(come_on.split())

    def characters(self, data):
        if self.begin_data:
            if self.check_data:
                self.description = self.description + self.get_out_char(data)

    def endElement(self, name):
        self.lgr.debug('endElement: %s', name)
        if name == 'xs:documentation':
            self.lgr.debug('xs:documentation')
            self.check_data = False

        elif name == 'xs:attribute':
            ci = ColumnInfo(self.name_column, self.use_column,
                self.description, self.type_column, self.length_column)
            self.arr_column.append(ci)
            self.description = ''
            self.lgr.debug('ok, add ci into arr_column')

    def startElement(self, name, attrs):
        self.node_name = name
        self.attrs = attrs

        if name == 'xs:attribute':
            self.begin_data = True
            self.lgr.debug('startElement xs:attribute ')
            if 'name' in attrs:
                name_is = attrs.getValue('name')
                self.lgr.debug('found column : %s', name_is)
                self.name_column = name_is

        elif name == 'xs:element':
            self.lgr.debug('startElement xs:element')
            if 'maxOccurs' in attrs:
                if 'name' in attrs:
                    name_element = attrs.getValue('name')
                    self.lgr.debug('startElement xs:element with value: %s',
                        name_element)
                    self.name_element = name_element

        elif name == 'xs:documentation':
            self.check_data = True

        elif name == 'xs:restriction':
            self.lgr.debug('xs:restriction')
            if 'base' in attrs:
                base = attrs.getValue('base')
                self.lgr.debug('xs:restriction base: %s', base)
                self.type_column = base

        elif name == 'xs:length':
            self.lgr.debug('xs:length')
            if 'value' in attrs:
                value = attrs.getValue('value')
                self.lgr.debug('xs:length value: %s', value)
                self.length_column = value

