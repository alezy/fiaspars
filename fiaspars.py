#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import os
from xdsparsing import FiasXdsParser
from dbcon import DBconnection
from filltable import FillTable
import logging
import argparse


class FiasParser():
    '''
    в одной папке распакованные  fias_xml.rar -> fias_xml
    Выгрузка из ФИАС в XML xsd схемы.zip -> Выгрузка из ФИАС в XML xsd схемы
    paht_to_xml -где лежат
    db_path - имя базы SQL (создается)
    '''

    class __TaskFile__():
        def __init__(self, name, xml_path, xds_path):
            self.name = name
            self.xml_path = xml_path
            self.xds_path = xds_path
            #для сортировки
            self.size_of = 0

        def __lt__(self, o: object) -> bool:
            return self.size_of < o.size_of

    def halt(self, msg, status=0):
        print('Exit!', msg)
        self.lgr.debug('halt message: %s Code: %s', msg, status)
        exit(status)

    def __init__(self, fias_folders, lgr=None):
        self.db_path = None
        self.lgr = lgr
        self.tasks = {}
        self.check_status = False

        if self.lgr is None:
            self.lgr = logging.getLogger('main')
            #self.lgr.setLevel(logging.DEBUG)
            self.lgr.setLevel(logging.ERROR)
            ch = logging.StreamHandler()
            #ch.setLevel(logging.DEBUG)
            ch.setLevel(logging.ERROR)
            formt = logging.Formatter('%(asctime)s - '
                                      '%(name)s - %(levelname)s - %(message)s')
            ch.setFormatter(formt)
            self.lgr.addHandler(ch)
            self.lgr.debug('set debug level...')

        if not os.path.exists(fias_folders):
            #self.lgr.critical('No folder - NO WAI ' + fias_folders)
            self.halt('No such file or directory...  ' + fias_folders, 1)
        else:
            self.db_path = fias_folders + '/fias.db'

        #arr of [size_of_xml_file, task]
        self.list_by_size = list()
        #key - table name, values - columns()
        self.dict_schema_table = {}
        xml_dict = {}

        def append_key(key_name, xml=None, xsd=None):
            if not key_name in xml_dict:
                xml_dict[key_name] = [xml, xsd]
            else:
                if not xml is None:
                    xml_dict[key_name][0] = xml
                if not xsd is None:
                    xml_dict[key_name][1] = xsd

        for root, dirs, files in os.walk(fias_folders):
            for _file in files:
                extension = _file[-3:].lower()
                full_path = root + '/' + _file
                name = _file[3: _file[3:].find('_') + 3]
                if name.upper() == 'DEL':
                    name = _file[3: _file[3:].find('_', len(name) + 1) + 3]

                if extension == 'xml':
                    append_key(name, xml=full_path)
                elif extension == 'xsd':
                    append_key(name, xsd=full_path)
        for key, val in list(xml_dict.items()):
            try:
                size = os.path.getsize(val[0])
            except:
                self.lgr.error('getsize_err: %s', val[0])
            fs = self.__TaskFile__(key, val[0], val[1])
            fs.size_of = size
            self.list_by_size.append([size, fs])

    def get_custom_columns(self, tb_names):
        custom_columns = ''

        def _get_column(tb_name, is_task=None):
            _result = ''
            _task = None
            if is_task is None:
                for task_item in self.list_by_size:
                    if task_item[1].name == tb_name:
                        _task = task_item[1]
                if _task is None:
                    self.lgr.critical('so task is None with tb_name %', tb_name)
                    exit(0)
            else:
                _task = is_task
            if _task.xds_path is None:
                self.lgr.critical('check the place xds files')
                exit(0)

            fxp = FiasXdsParser(_task.xds_path, self.lgr)
            ci_arr = fxp.getAllcolumns()
            _result = '\nName TB: ' + _task.name + '\n'
            if not _task.name in self.dict_schema_table:
                self.dict_schema_table[_task.name] = ci_arr

            for ci in ci_arr:
                _result += ci.name.ljust(16) + '# ' + ci.desc + '\n'
            _result += '=' * 40 + '\n'
            return _result

        if not tb_names:
            self.lgr.debug('tb_names is clean')
            for item in self.list_by_size:
                task = item[1]
                self.lgr.debug('work with %s table', task.name)
                custom_columns += _get_column(task.name, task)

        else:
            for table in tb_names:
                if self.table_exist(table):
                    custom_columns += _get_column(table)

        return custom_columns

    def getSql(self, fxp, name_tb, table_name=None, columns=None):
        def _getTypeSqlite(xs_type):
            if xs_type == 'xs:string':
                return 'TEXT'
            elif xs_type == 'xs:integer' or xs_type == 'xs:byte' or xs_type == 'xs:int':
                return 'INTEGER'
            else:
                self.lgr.critical('unknown type: %s', xs_type)
                return 'UNKNOWN: ' + xs_type

        ci_arr = fxp.getAllcolumns()
        if table_name is not None:
            if name_tb != table_name:
                self.lgr.critical('name_tb != table_name')

        self.lgr.debug('Create table %s', name_tb)
        SQLSchema = 'CREATE TABLE if not exists ' + name_tb + ' (\n'
        for ci in ci_arr:
            if columns is not None:
                if ci.name in columns:
                    self.lgr.debug('Custom column - %s', ci.name)
                else:
                    self.lgr.debug('skip other column - %s', ci.name)
                    continue
            SQLSchema += '\t--' + ci.desc + '\n\t' + ci.name + ' ' \
                         + _getTypeSqlite(ci.type_clmn)
            if ci.use == 'required':
                SQLSchema += ' NOT NULL'
            SQLSchema += ',\n'
        SQLSchema = SQLSchema[:-2] + ');'
        self.lgr.debug('full sql: \n\t %s', SQLSchema)
        return SQLSchema

    def create_tables(self, fxp, name_tb, db_conn, table_name=None,
                      columns=None):
        if table_name is not None and columns is not None:
            SQLSchema = self.getSql(fxp, name_tb, table_name,
                                    columns)
        else:
            SQLSchema = self.getSql(fxp, name_tb)
        db_conn.createDB(name_tb, SQLSchema)

    def table_exist(self, table_name):
        status = False
        tables = ''
        for item in self.list_by_size:
            tables += item[1].name + ', '
            if table_name == item[1].name:
                status = True
                self.lgr.debug('table %s exist:', table_name)
        if not status:
            if not self.lgr is None:
                self.lgr.critical('%s table is not exist available tables: %s',
                                  table_name, tables)
        return status

    def columns_exist(self, table_name, columns_arr):
        if not table_name in self.dict_schema_table:
            self.get_custom_columns((table_name, ))
        ci_arr = self.dict_schema_table[table_name]
        column = []
        for ci in ci_arr:
            column.append(ci.name)
        for col in columns_arr:
            try:
                column.index(col)
            except ValueError:
                self.lgr.critical('%s - is not valid column from %s table',
                                  col, table_name)
                print('available: ', column)
                exit(0)

            else:
                self.lgr.debug('%s is valid column for tb %s',
                               col, table_name)
        return True

    def show_columns(self, columns):
        self.lgr.debug('showcolumns is %s', columns)
        _result = ''
        for xml in columns:

            self.lgr.debug('name xml %s', xml)
            if not xml:
                # -s
                self.lgr.debug('show all...')
                _result += self.get_custom_columns(xml)
            else:
                # -s HOUSE ADDROBJ etc
                _result += self.get_custom_columns(xml)

        return _result

    def fill_table(self, trigger_key, task):
        self.lgr.debug('start fill table: %s', task.name)
        db_conn = DBconnection(task.name, self.db_path, self.lgr)
        try:
            FillTable(task.xml_path, trigger_key, db_conn, self.lgr, check_status=self.check_status)
        finally:
            db_conn.disconnect()

    def fill_table_with_custom_columns(self, trigger_key, task, columns):
        self.lgr.debug('start fill table: %s', task.name)
        db_conn = DBconnection(task.name, self.db_path, self.lgr)
        try:
            FillTable(task.xml_path, trigger_key, db_conn, self.lgr, columns, check_status=self.check_status)
        finally:
            db_conn.disconnect()

    def create_fill_table(self, table_name=None, columns=None):
        changed = False
        for i in self.list_by_size:
            task = i[1]
            if not table_name is None:
                if not table_name == task.name:
                    continue
                if not columns:
                    print('ok add all columns...')

                    if not table_name in self.dict_schema_table:
                        self.get_custom_columns((table_name, ))
                    ci_arr = self.dict_schema_table[table_name]
                    columns = []

                    for ci in ci_arr:
                        columns.append(ci.name)
                    changed = True
            self.lgr.debug('task: \n\tsize_of: %s, \n\tname: %s, \n\txml:'
                           ' %s \n\txds: %s', task.size_of, task.name,
                           task.xml_path, task.xds_path)
            fxp = FiasXdsParser(task.xds_path, self.lgr)
            db_conn = DBconnection(task.name, self.db_path, self.lgr)

            if table_name is not None and columns is not None:
                try:
                    self.create_tables(fxp, task.name, db_conn,
                                       table_name, columns)
                finally:
                    db_conn.disconnect()
            else:
                try:
                    self.create_tables(fxp, task.name, db_conn)
                finally:
                    db_conn.disconnect()

            trigger_key = fxp.getTriggerName()
            if trigger_key is None:
                self.halt('No key no way')
            self.lgr.debug('fill_table: %s', task.name)

            if columns is None:
                self.fill_table(trigger_key, task)

            else:
                self.columns_exist(table_name, columns)
                self.lgr.debug('custom fill')
                if changed:
                    self.fill_table_with_custom_columns(trigger_key, task, [])
                else:
                    self.fill_table_with_custom_columns(trigger_key, task, columns)

    def show_sql(self):
        if len(self.list_by_size) == 0:
            return None
        else:
            sql = ''
            for i in self.list_by_size:
                task = i[1]
                fxp = FiasXdsParser(task.xds_path, self.lgr)
                db_conn = DBconnection(task.name, self.db_path, self.lgr)
                sql += self.getSql(fxp, task.name)

            return sql

def create_parser():
    logger = logging.getLogger('main')
    logger.setLevel(logging.ERROR)
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    formt = logging.Formatter('%(asctime)s - '
                              '%(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formt)
    parser = argparse.ArgumentParser()

    parser.add_argument('--showsql', '-sql', action='store_true',
                        help='show full sql query EXAMPLE ')
    parser.add_argument('fias_folders', action='store',
                        help='path to extract xds and xml files')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='set loglevel verbose')
    parser.add_argument('-c', '--customcolumns', nargs='*', action='append',
                        help='parsing only selected XML files ' +
                             '\n Example: -c TABLENAME COLUNMSNAMES ' +
                             ' -c ADDROBJ AOGUID SEXTCODE')
    parser.add_argument('-s', '--showcolumns', nargs='*', action='append',
                        help='show columns in XML files EXAMPLE -s ADDROBJ ACTSTAT')
    parser.add_argument('-o', '--onlyactive', action='store_true',
                        help='insert only valid data')

    args = parser.parse_args()
    if args.verbose:
        logger.setLevel(logging.DEBUG)
        ch.setLevel(logging.DEBUG)

    logger.addHandler(ch)

    fp = FiasParser(args.fias_folders, logger)
    fp.list_by_size.sort()
    fp.list_by_size.reverse()

    if args.showsql:
        print(fp.show_sql())
        exit(0)

    if not args.showcolumns is None:
        print(fp.show_columns(args.showcolumns))
        exit(0)

    if args.onlyactive:
        fp.check_status = True

    if not args.fias_folders is None:
        if args.customcolumns is None:
            logger.debug('parsing all xml start. Amen')
            fp.create_fill_table()
        else:
            logger.debug('Ok try exec with custom columns')
            if args.customcolumns == [[]]:
                print('usage: -c table_name column1 column2 column3')
                exit(0)
            for param in args.customcolumns:
                prefx = param[0].find('_')
                if prefx == -1:
                    table = param[0]
                else:
                    table = param[0][prefx + 1:]
                columns = param[1:]
                fp.table_exist(table)
                status = fp.columns_exist(table, columns)
                if status:
                    fp.create_fill_table(table, columns)


if __name__ == '__main__':
    create_parser()

