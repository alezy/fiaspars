#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser, handler
from time import time
import logging

def getStatus(self, dict_attrs):
    '''
    проверка актуальности
    ACTSTATUS INTEGER,
    Статус актуальности адресного объекта ФИАС. Актуальный адрес
    на текущую дату. Обычно последняя запись об адресном объекте.
    0 – Не актуальный1 - Актуальный
    '''

    triggers = ['House', 'Object', 'HouseInterval', 'NormativeDocument', 'Landmark', 'AddressObjectType',
                'CurrentStatus', 'HouseStateStatus', 'NormativeDocumentType', 'OperationStatus',
                'CenterStatus', 'StructureStatus', 'IntervalStatus', 'EstateStatus', 'ActualStatus']

    result = False
    if 'ACTSTATUS' in dict_attrs:
        if dict_attrs['ACTSTATUS'] == '1':
            result = True
        else:
            self.lgr.debug('not active status ', dict_attrs['ACTSTATUS'])
    else:
        self.lgr.debug('No current status in ', dict_attrs.keys())
        #магия
        result = True
    return result


class FillTable():
    def __init__(self, xml_file_name, trigger_key, conn, logger,
                 columns_arr=None, check_status=False):
        self.xml = xml_file_name
        self.trigger_key = trigger_key
        self.lgr = logger.getChild('filltable')

        self.db_connection = conn
        parser = make_parser()
        if columns_arr is None:
            parser.setContentHandler(_XmlHandler(trigger_key, self.db_connection,
                                                 self.lgr, check_status))
        else:
            parser.setContentHandler(CustomColumnsHandler(trigger_key, self.db_connection,
                                                          columns_arr, self.lgr, check_status))
        parser.parse(open(self.xml, 'r', encoding='utf-8-sig'))


class CustomColumnsHandler(handler.ContentHandler):
    def __init__(self, trigger_key, conn, columns_array, lgr, check_status):

        self.trigger_key = trigger_key
        self.conn = conn
        self.lgr = lgr
        self.columns_arr = columns_array
        self.check = check_status

        if not self.columns_arr:
            print('Add all columns...')

        self._data = {}

    def startElement(self, name, attrs):
        cust_attrs = {}
        if not self.columns_arr:
            self._data = attrs
        else:
            self.lgr.debug('startElement: %s', name)
            for key, val in list(attrs.items()):
                if key in self.columns_arr:
                    cust_attrs[key] = val
                self.lgr.debug('key: %s; val:%s', key, val)
            self._data = cust_attrs

    def endElement(self, name):
        if name == self.trigger_key:
            self.lgr.debug('name %s == trigger_key %s -> do insert:',
                           name, self.trigger_key)
            if self._data:
                if self.check:
                    result = getStatus(self, self._data)
                    if not result:
                        self.lgr.debug('skip invalid data (')
                    else:

                        self.conn.insertDictIntoTb(self._data)
            else:
                self.conn.insertDictIntoTb(self._data)

    def endDocument(self):
        self.lgr.debug('Nothing to do there... committing')
        self.conn.commit()


class _XmlHandler(handler.ContentHandler):
    def __init__(self, trigger_key, conn, lgr, check_status):
        self.trigger_key = trigger_key
        self.conn = conn
        self.lgr = lgr
        self._data = {}
        self.check = check_status
        self.count = 0
        self.start = time()
        self.is_debug = self.lgr.isEnabledFor(logging.DEBUG)

    def startElement(self, name, attrs):
        if self.is_debug:
            self.lgr.debug('startElement: %s', name)
            for key, val in list(attrs.items()):
                self.lgr.debug('key: %s; val:%s', key, val)
        self._data = attrs

    def endElement(self, name):
        if name == self.trigger_key:
            self.count += 1
            if self.count % 100000 == 0:
                print('{0}: {1:>12} -> {2} sec per mln'.format(self.trigger_key, self.count, int(1000000*(time() - self.start)/self.count)))

            if self.is_debug:
                self.lgr.debug('name %s == trigger_key %s -> do insert:', name, self.trigger_key)
            if self.check:
                result = getStatus(self, self._data)
                if not result:
                    if self.is_debug:
                        self.lgr.debug('skip invalid data (')
                else:
                    self.conn.insertDictIntoTb(self._data)
            else:
                self.conn.insertDictIntoTb(self._data)

    def endDocument(self):
        self.lgr.debug('Nothing to do there... committing')
        self.conn.commit()
